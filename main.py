import sys
import MainWindow
from PySide2 import QtWidgets


if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    widget = MainWindow.MainWindow()
    widget.resize(800, 600)
    widget.show()

    sys.exit(app.exec_())

