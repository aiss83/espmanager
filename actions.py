from PySide2 import QtWidgets


class ActionsRegistry:
    """ Global actions registry to interact with user interface """

    def __init__(self):
        self.actions = []

    def addAction(self, action):
        self.actions.append(action)
