from PySide2 import QtWidgets, QtGui
import MainPanel


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

        # Setup toolbar
        self.toolBar = self.createToolBar()
        self.addToolBar(self.toolBar)

        self.createMenus()
        statusBar = self.createStatusBar()

        self.mainPanel = MainPanel.MainPanel()
        self.setCentralWidget(self.mainPanel)

        self.setStatusBar(statusBar)

    """
    Create program menus
    """
    def createMenus(self):
        if not self.menuBar() is None:
            helpMenu = self.menuBar().addMenu("&Help")

    """
    Create program toolbar
    """
    def createToolBar(self):
        toolbar = QtWidgets.QToolBar()
        # Add some actions here
        return toolbar

    """
    Create some user-defined actions
    """
    def createActions(self):
        return None

    def createStatusBar(self):
        statusBar = QtWidgets.QStatusBar()
        self.statusBarText = QtWidgets.QLabel('Ready')
        statusBar.addWidget(self.statusBarText)

        return statusBar
